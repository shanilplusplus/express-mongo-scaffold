import express from "express";
import cors from "cors";
import mongoose from "mongoose";
const app = express();
import recipeRoute from "./routes/recipes";
require('dotenv').config();


app.use(cors())
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);



mongoose.connect(process.env.DB_MONGO, () =>{
    console.log("db connect4ed")
})

app.use('/recipes', recipeRoute)

app.get('/', async (req,res) => {
    console.log('hello from api');

    res.send({result: 'A'})

})


app.get('*', (req,res) => {
    res.send("hello im under the water")
})

app.listen(5000, () => {
    console.log("server is ready");
})