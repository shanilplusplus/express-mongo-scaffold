import express from "express"; 
const router = express.Router();
import Recipe from "../models/Recipe";



router.get('/', async (req,res) => {
  
    try{
        const recipes = await Recipe.find();
        res.json(recipes);

    }catch(e){
        res.json({e})
    }
})

router.get('/:id', async (req,res) => {
    console.log(req.params.id)
    
    try{
        const recipe = await Recipe.findById(req.params.id);
        res.json(recipe);
    } catch(err){
        res.json({message: err})
    }
})

router.delete('/:id', async (req,res) => {
    try{
        const removedRecipe = await Recipe.remove({_id: req.params.id})
        res.json(removedRecipe)
    } catch(err){

    }
})

router.post('/', async (req,res) => {
  
    // console.log(req)
    // const recipe = new Recipe({
    //     title: "Sri Lankan Christmas Cake",
    //     author:"Shaun Sigera",
    //     content:"For years I firmly believed that I had tried every single variation of the Christmas cake possible. Light, dark, moist, dry, British, Scottish, Italian, Serbian… That was, until I met my Sri Lankan husband. I would have never thought that the richest, the most decadent, the most interesting and the most delicious Christmas cake of all would come from Sri Lanka. Being a commonwealth country, Sri Lankans inherited Christmas cake from their British rulers, but then turned it into something quite unique, by adding exotic spices and native fruits. For several years, our cake would arrive promptly before Christmas, a tiny precious piece of it, wrapped in a foil and neatly packed by my in-laws. We would strive to keep it for as longs as possible, taking a bite after dinner every night and then fighting over who gets the last piece.",
    //     image:"https://images.food52.com/dNcYxiiP9dCSMFKfnFLzNXVFHKk=/1008x672/filters:format(webp)/e5838f3c-662d-4adf-b1f2-0db4e1429ab9--sri_lankan_christmas_cake.jpg",
    //     rating: 5
    // })

    // try{
    //     const dbPost = await recipe.save()
    //     res.json(dbPost)
 
    // } catch(e){
    //     res.json({e})
    // }


   
  
    console.log(req.body)
})


export default router; 