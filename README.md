# express-mongo-scaffold

exress api routes with mongo mongoose scaffold.

### Prerequisites

you need to have node installed in your computer. 



## Getting Started

to get started on you local machine, clone this repo, and install all the node modules. Then bundle a production ready copy using webpack. Finally, fire up the node server.


### Installing
cd into the folder
install npm dependencies


### Running Locally

```
npm install
npm run start

```

## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [Express](https://webpack.github.io/) - bundling
* [Mongodb](https://rometools.github.io/rome/) - compiling and transpling
* [Mongoose](https://rometools.github.io/rome/) - ORM



## Authors

* **Shaun Sigera** (http://shanilplusplus.com/)


## License

This project is licensed under the MIT License
