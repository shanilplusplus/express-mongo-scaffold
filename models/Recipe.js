import mongoose from "mongoose";



const RecipeSchema = mongoose.Schema({
    title: String,
    author: String,
    content: String,
    image: String,
    rating:Number,
    date: { type: Date, default: Date.now },

})

module.exports = mongoose.model('Recipes', RecipeSchema)